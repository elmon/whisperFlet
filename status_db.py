#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
import sys, os, platform, time, pprint
import csv, sqlite3, statistics
from pathlib import Path
from sqlite3 import Error
from datetime import datetime

DB_FILE = Path(__file__).resolve().parent / 'status_data.db'

dirTS = {}
conn = None
cur = None

SQL_CREATE_DATA_TABLE = """CREATE TABLE IF NOT EXISTS `estimate` (
    `id`	INTEGER PRIMARY KEY AUTOINCREMENT,
    `datum`    TEXT DEFAULT '',
    `sprache`  TEXT DEFAULT '',
    `model_size`  TEXT DEFAULT '',
    `app`  TEXT DEFAULT '',
    `system`  TEXT DEFAULT '',
    `node`  TEXT DEFAULT '',
    `machine`  TEXT DEFAULT '',
    `dauer`	  REAL DEFAULT 0.0,
    `zeit`	  REAL DEFAULT 0.0,
    `tps`	  REAL DEFAULT 0.0
);"""


SQL_ESTIMATE_GESAMT = """SELECT model_size, app, sprache, system, node, machine,
    avg(tps) as AvgVal, min(tps) as MinVal, max(tps) as MaxVal, count(*) as cnt
    FROM estimate 
    GROUP BY model_size, app, sprache, system, node, machine
    ORDER BY model_size, app, sprache
;"""

SQL_ESTIMATE_APP = """SELECT model_size, app, sprache,
    avg(tps) as AvgVal, min(tps) as MinVal, max(tps) as MaxVal, count(*) as cnt
    FROM estimate 
    GROUP BY model_size, app, sprache
    ORDER BY model_size, app, sprache
;"""

SQL_ESTIMATE = """SELECT avg(tps) as AvgVal, min(tps) as MinVal, max(tps) as MaxVal, count(*) as cnt
    FROM estimate 
    where 
    sprache = ? and  model_size = ? and app = ?
    GROUP BY sprache, model_size, app
;"""

SQL_TPS_LIST = """SELECT tps
    FROM estimate 
    where 
    sprache = ? and  model_size = ? and app = ?
;"""


def status_to_db(sprache, model_size, app, dauer, zeit, tps): 
    if not os.path.isfile(DB_FILE):
        print('-> erstelle DB', DB_FILE)    
        sqlite_execute(SQL_CREATE_DATA_TABLE)

    datum = datetime.now().strftime("%Y%m%d")
    try:
      zeit = float(zeit)
      dauer = float(dauer)
      tps = zeit/dauer
    except:
      return

    sql_stmt = "INSERT INTO estimate (datum, sprache, model_size, app, \
                system, node, machine, \
                dauer, zeit, tps) \
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

    liParam = [datum, sprache, model_size, app, 
        platform.system() , platform.node(), platform.machine(), 
        dauer, zeit, tps]

    sqlite_execute(sql_stmt, liParam)


def get_tps_avg(sprache, model_size, app):
    initializeDB()
    try:
      conn = sqlite3.connect(DB_FILE)
      cur = conn.cursor()
    
      liParam = [sprache, model_size, app]
      cur.execute(SQL_ESTIMATE, liParam)

      rows = list(cur)
    
    except Error as e:
      print('Fehler zeigeErgebnisDB(): %s '% str(e))
    finally:
      conn.close()

    if len(rows):
      return rows[0]
    else:
      return (0, 0, 0, 0)


def get_tps_median(sprache, model_size, app):
    initializeDB()
    try:
      conn = sqlite3.connect(DB_FILE)
      cur = conn.cursor()
    
      liParam = [sprache, model_size, app]
      cur.execute(SQL_TPS_LIST, liParam)

      rows = [row[0] for row in cur.fetchall()]
    
    except Error as e:
      print('Fehler zeigeErgebnisDB(): %s '% str(e))
    finally:
      conn.close()

    if len(rows):
      return (statistics.median(rows), min(rows), max(rows), len(rows))
    else:
      return (0, 0, 0, 0)


def sqlite_execute(sql_stmt='', liParam=[]):
    try:
      conn = sqlite3.connect(DB_FILE)
      cur: cur = conn.cursor()

      if liParam:
        cur.execute(sql_stmt, liParam)
      else:
        cur.execute(sql_stmt)
      # print('sqlite_execute ->', sql_stmt)
      # print('cur.rowcount -> ', cur.rowcount)
      # print('liParam ->', liParam)
      conn.commit()
    except Error as e:
      print('Fehler sqlite_execute(): %s '% str(e))
      print('-> %s '% sql_stmt)
      if liParam: print('param->',liParam)
    finally:
      conn.close()


def initializeDB():
    if not os.path.isfile(DB_FILE):
        print('-> initializeDB: erstelle DB', DB_FILE)    
        sqlite_execute(SQL_CREATE_DATA_TABLE)


def main():
    initializeDB() 
    
    # status_to_db(sprache, model_size, app, dauer, zeit, tps)
    status_to_db('de', 'tiny', 'faster', 60, 10, 10/60)
    status_to_db('de', 'tiny', 'faster', 60, 20, 20/60)
    status_to_db('de', 'tiny', 'faster', 60, 40, 40/60)
    
    ret = get_tps_avg('de', 'tiny', 'faster') 
    print(ret)
    ret = get_tps_median('de', 'tiny', 'faster')
    print(ret)
    
            
if __name__ == "__main__":
  main()        
 

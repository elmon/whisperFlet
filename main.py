#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 10 12:12:39 2023

@author: elmon

https://github.com/openai/whisper
https://github.com/guillaumekln/faster-whisper

sudo apt install ffmpeg

pip install faster-whisper
pip install -U openai-whisper

pip instal tomli  # bis 3.11
pip install tomli_w

pip install flet
sudo apt-get install zenity

flet build linux  <-- mit Flutter

pip install pyinstaller
flet pack main.py --name whisperFlet
"""

import os, sys, time
from datetime import datetime
from pathlib import Path
from status_db import status_to_db, get_tps_median
import flet as ft
import whisper_flet as wf
import tomli_w, tomli as tomlib

# logging
import logging
lfile = 'whisper-flet.log'
#print('--> log-file -> ', lfile)

logging.basicConfig(filename=lfile,
    format='%(asctime)s %(levelname)s: %(message)s',
    encoding='utf-8', level=logging.ERROR)
logging.debug('starte main.py')

config = {}
conf_path = Path(__file__).parent / "config.toml"

if os.path.isfile(conf_path):
    config = tomlib.loads(conf_path.read_text(encoding="utf-8"))


def write_config():
    with open(conf_path, mode="wb") as fp:
        tomli_w.dump(config, fp)


def get_diff(diff):
    sto = time.gmtime(diff)
    if sto.tm_hour > 0:
        return time.strftime("%H:%M:%S", sto)
    else:
        return time.strftime("%M:%S", sto)
    
    
def main(page: ft.Page):
    page.scroll = "adaptive"
    page.title = "Transkribiere Audio/Video"
    page.spacing = 10
    page.padding = 10
    page.window_top = 100
    page.window_left = 100
    #page.window_height = 800
    page.window_width = 675
    page.window_min_width = 500

    text = ft.Text(value="Transkribiere Audio/Video", size=30)

    tf_tvt = ft.TextField(label="Transkript", disabled=True, multiline=True, read_only=True, value="")
    tf_log = ft.TextField(label="Log", disabled=True, multiline=True, max_lines=3, read_only=True, value="")
    tf_status = ft.TextField(label="Status", disabled=True, multiline=True, max_lines=3, read_only=True, value="")

    #tf_file = ft.TextField(label="wähle ein audio file", read_only=True, value="")
    tf_file = ft.TextField(label="audio/video", disabled=True, read_only=True, multiline=True, value="")


    def button_clicked(e):
        starte_whisper()


    button = ft.ElevatedButton(
        content=ft.Text(value="Start", size=25),
        disabled=True,
        on_click=button_clicked,
        # bgcolor=ft.colors.BLUE_800
    )

    # file picker
    file_name = ''
    file_path = ''
    audio_file = ''
    audio_size = 0
    audio_dauer = 0

    def pick_files_result(e: ft.FilePickerResultEvent):
        global audio_file, audio_size
        button.disabled = True if e.files is None else False
        # print('--> button:', button.disabled, e.files)
        # print("Selected files:", e.files)
        button.update()
        # print("Selected file or directory:", e.path)
        if e.files:
            tf_file.disabled = False
            tf_file.value = e.files[0].name if e.files else "Cancelled!"
            audio_file = e.files[0].path
            audio_size = e.files[0].size
            file_name = e.files[0].name
            file_path = os.path.dirname(e.files[0].path)
            #print('--> audio_file:', audio_file)
            button_mp3.disabled = (file_name[-3:].lower() == 'mp3')
            page.update()
            add_status()
            config['initial_path'] = file_path
            write_config()
        else:
            tf_file.value = ''
            tf_file.disabled = True
            tf_status.value = ''
            tf_status.disabled = True
            audio_file = ''
            audio_size = 0
            tf_file.update()

    pick_files_dialog = ft.FilePicker(on_result=pick_files_result)

    page.overlay.append(pick_files_dialog)

    button_file = ft.ElevatedButton(
            "Wähle Audio / Video",
            icon=ft.icons.FOLDER_OPEN,
            on_click=lambda _: pick_files_dialog.pick_files(
                allow_multiple=False,
                initial_directory=config.get('initial_path', ''),
                file_type=ft.FilePickerFileType.CUSTOM,        # this option is needed for the 'allowed_extensions' to work !
                allowed_extensions=['3gp', 'aac', 'flv', 'm4a', 'mp3', 'mp4', 'ogg', 'wav', 'webm']              # the file picker mainly show csv files and not all files which is the default
            )
        )


    pr = ft.ProgressRing()
    pr.value = 0.0
    pr_mp3 = ft.ProgressRing(width=8, height=8)
    pr_mp3.value = 0.0

    def button_mp3_clicked(e):
        global audio_file
        button_mp3.disabled = True
        pr_mp3.value = None
        add_log(f'Konvertiere {tf_file.value} nach mp3')
        ret = wf.video_to_mp3(audio_file)
        pr_mp3.value = 0.0
        if not ret:
            ret = f'{tf_file.value[:-4]}.mp3 erstellt'
        add_log(ret)
        

    button_mp3 = ft.ElevatedButton(
        ' --> mp3',
        tooltip='video --> mp3',
        disabled=True,
        icon=ft.icons.MUSIC_VIDEO,
        # bgcolor=ft.colors.BLUE_600,
        on_click=button_mp3_clicked
        )


    row_file = ft.Row(
        spacing=5, 
        controls=[tf_file, button_file, pr_mp3, button_mp3],
    )


    def starte_whisper():
        global audio_file, audio_dauer
        button.disabled = True
        button_file.disabled = True
        tf_tvt.value = ''
        tf_tvt.disabled = True
        pr.value = None
        starte = f"starte: {dd_lang.value} / {dd_model.value} / {dd_alternative.value} / {tf_file.value}"
        add_log(starte)
        # li_ext = ['txt', 'tvt', 'vtt', 'srt', 'lrc']
        if not config.get('write_extension',''):
            config['write_extension'] = ['txt']
            write_config()   
        try:
            if dd_alternative.value == 'openai':
                segments, status = wf.transcribe_whisper(audio=audio_file, model_size=dd_model.value, language=dd_lang.value.replace('-', ''), li_ext=config['write_extension'])
            elif  dd_alternative.value == 'faster':
                segments, status = wf.transcribe_fstr_whisper(audio=audio_file, model_size=dd_model.value, language=dd_lang.value.replace('-', ''), li_ext=config['write_extension'])
            else:    
                status = 'keine passende Alternative ausgewählt!'
                segments = ''
        except:
            segments = ''
            status = f"Fehler: {sys.exc_info()[1]}"
            logging.critical(status)

        pr.value = 0.0
        button.disabled = False  
        button_file.disabled = False
        add_log(status)   
        add_tvt(segments)    
        status_to_db(dd_lang.value, dd_model.value, dd_alternative.value, audio_dauer, status, 0)   


    def add_log(text):
        global audio_dauer
        tf_log.disabled = False
        log = tf_log.value
        jetzt = datetime.now().strftime("%d.%m.%Y %H:%M:%S")
        if type(text) is not str:
            tps = int(100 * text / audio_dauer) / 100
            text = f'verstrichene Zeit: {get_diff(text)} ({tps})'  
        tf_log.value = f"[{jetzt}] {text}\n{log}"      
        page.update()


    def add_tvt(segments):
        liText = []
        if segments:
            for segment in segments:
                if type(segment) is dict:
                    start = segment['start']
                    end = segment['end']
                    text = segment['text'].strip()
                else:
                    start = segment.start
                    end = segment.end
                    text = segment.text.strip()
                liText.append(f"{get_diff(start)}  {text}")   
            tf_tvt.disabled = False         
        else:
            tf_tvt.disabled = True

        tf_tvt.value = '\n'.join(liText)
        tf_tvt.update()


    def add_status():
        global audio_file, audio_size, audio_dauer
        liTps = get_tps_median(dd_lang.value, dd_model.value, dd_alternative.value)
        liText = []
        liText.append(f'Pfad: {audio_file}')
        size = int(audio_size * 100 / pow(1024,2))
        audio_dauer = wf.get_duration(audio_file)
        liText.append(f'Größe: {size / 100} MB / Dauer: {get_diff(audio_dauer)}')
        estimate = f'{get_diff(audio_dauer*liTps[0])} [ {get_diff(audio_dauer*liTps[1])} - {get_diff(audio_dauer*liTps[2])} ] ({int(liTps[0]*100)/100})'
        liText.append(f'Geschätzte Zeit: {estimate}')
        liTps = [int(x*100)/100 for x in liTps]
        liText.append(f'tps: {liTps}')
        
        tf_status.disabled = False
        tf_status.value = '\n'.join(liText)
        tf_status.update()

    def get_tps_simple():
        model_size = dd_model.value
        if model_size ==  'tiny':
            tps = 0.34
        elif model_size ==  'base':
            tps = 0.51
        elif model_size ==  'small':
            tps = 1.45
        elif model_size ==  'medium':
            tps = 5.0
        elif model_size ==  'large':
            tps = 0.0
        elif model_size ==  'turbo':
            tps = 0.0    
        else:
            tps = 0.0        
        return tps               


    def dd_changed(e):
        config[e.control.label] = e.control.value 
        if tf_file.value:
            add_status()
        write_config()


    dd_lang = ft.Dropdown(
        label="Sprache",
        width=150,
        on_change=dd_changed,
        options=[
            ft.dropdown.Option("-"),
            ft.dropdown.Option("de"),
            ft.dropdown.Option("en"),
            ft.dropdown.Option("fr"),
            ft.dropdown.Option("es"),
            ft.dropdown.Option("it"),
            ft.dropdown.Option("ro"),
            ft.dropdown.Option("sv"),
        ],
        value=config.get("Sprache", "de")
    )

    dd_model = ft.Dropdown(
        label="model",
        width=150,
        on_change=dd_changed,
        options=[
            ft.dropdown.Option("tiny"),
            ft.dropdown.Option("base"),
            ft.dropdown.Option("small"),
            ft.dropdown.Option("medium"),
            ft.dropdown.Option("large"),
            ft.dropdown.Option("turbo"),
        ],
        value=config.get("model", "tiny")
    )
    
    dd_alternative = ft.Dropdown(
        label="Alternativen",
        width=150,
        on_change=dd_changed,
        options=[
            ft.dropdown.Option("openai"),
            ft.dropdown.Option("faster"),
        ],
        value=config.get("Alternativen", "openai")
    )

    row_param = ft.Row(
        spacing=10, 
        controls=[dd_lang, dd_model, dd_alternative, pr, button],
    )

    page.add(text)
    page.add(row_file)
    page.add(row_param)
    page.add(tf_status)
    page.add(tf_log)
    page.add(tf_tvt)
    page.update()

ft.app(target=main)

#!/bin/bash
# startet venv und erstellt es, sofern nicht vorhanden

REQ_TXT="requirements.txt"
PATH_TO_VENV="venv"

# auf pip prüfen und installieren
if which "pip" &> /dev/null; then
    echo "-> pip ist vorhanden."
else
    echo "-> pip nicht vorhanden!"
    if which "python3" &> /dev/null; then      # Debian / Ubuntu
        echo "  --> sudo apt install python3-pip"
        sudo apt install -y python3-pip
        sudo apt install -y python3-venv
    elif which "python" &> /dev/null; then     # ARCH ???
        echo "  --> sudo pacman python-pip"    
        sudo pacman -S --noconfirm python-pip
        sudo pacman -S --noconfirm python-venv
    else
        echo "  --> python nicht vorhanden ?!"
    fi
fi


# venv aktivieren
echo "-> source $PATH_TO_VENV/bin/activate ?"
if [ -d "$PATH_TO_VENV" ]; then
    echo "-> $PATH_TO_VENV vorhanden."
    if [ -e "$PATH_TO_VENV/bin/activate" ]; then
        echo "--> source $PATH_TO_VENV/bin/activate"
        source "$PATH_TO_VENV/bin/activate"
    else
        echo "--> $PATH_TO_VENV/bin/activate fehlt!"
    fi
else
    echo "-> $PATH_TO_VENV nicht vorhanden!"
    if which "python3" &> /dev/null; then
        echo "--> python3 -m venv $PATH_TO_VENV"
        python3 -m venv "$PATH_TO_VENV"
    elif which "python" &> /dev/null; then
        echo "--> python -m venv $PATH_TO_VENV"
        python -m venv "$PATH_TO_VENV"
    fi
    echo "--> source $PATH_TO_VENV/bin/activate"
    source "$PATH_TO_VENV/bin/activate"

    # upgrade pip
    echo "--> pip install --upgrade pip"
    "$PATH_TO_VENV/bin/pip" install --upgrade pip

    # requirements installieren
    if [ -f $REQ_TXT ]; then
        echo "--> pip install -r $REQ_TXT"
        "$PATH_TO_VENV/bin/pip" install -r $REQ_TXT
    else
        echo "--> $REQ_TXT nicht vorhanden!"
    fi
fi

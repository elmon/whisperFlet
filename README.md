# whisperFlet

GUI in Flet for [openai-whisper](https://github.com/openai/whisper) and [faster-whisper](https://github.com/guillaumekln/faster-whisper)  
to transkribe audio / video

![screenshot whisperFlet](https://codeberg.org/elmon/whisperFlet/raw/commit/e965fc689a385703c9f996afee6b94cf7d201bbd/Screenshot_whisperFlet_klein.png)

## Features
- Geschätze transkribierdauer durch errechnen des Median gemachter Transkriptionen
- Ausgabe des Transkripts in den Formaten TXT VTT SRT LRC, einstellbar in config.toml

### install unter debian / ubuntu
```bash
git clone https://codeberg.org/elmon/whisperFlet.git
cd whisperFlet

sudo apt install -y ffmpeg

sudo apt install -y python3-pip
sudo apt install -y python3-venv
```

### venv erstellen und aktivieren
```
python3 -m venv venv
source venv/bin/activate
```

### python-packete installiern
```python
pip install -U openai-whisper
pip install -U faster-whisper
pip install -U flet
pip install tomli  # bis python 3.11
pip install tomli_w
```
oder  
`pip install -r requirements.txt`

### run
```
source venv/bin/activate
python main.py
```
oder  
`venv/bin/python main.py`



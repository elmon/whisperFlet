#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 10 12:12:39 2023

@author: elmon

https://github.com/openai/whisper
https://github.com/guillaumekln/faster-whisper

sudo apt install ffmpeg
pip install faster-whisper
pip install -U openai-whisper
"""

import os, sys, datetime, time
import subprocess, pathlib
import whisper
from faster_whisper import WhisperModel

# logging
import logging
lfile = 'whisper-flet.log'
#print('--> log-file -> ', lfile)


logging.basicConfig(filename=lfile,
    format='%(asctime)s %(levelname)s: %(message)s',
    encoding='utf-8', level=logging.ERROR)
logging.debug('starte whisper_flet.py')


def format_timestamp(
    seconds: float,
    always_include_ms: bool = False,
    always_include_hours: bool = False,
    decimal_marker: str = "."
):
    assert seconds >= 0, "non-negative timestamp expected"
    milliseconds = round(seconds * 1000.0)

    hours = milliseconds // 3_600_000
    milliseconds -= hours * 3_600_000

    minutes = milliseconds // 60_000
    milliseconds -= minutes * 60_000

    seconds = milliseconds // 1_000
    milliseconds -= seconds * 1_000

    ms_marker = f"{decimal_marker}{milliseconds:03d}" if always_include_ms else ""
    hours_marker = f"{hours:02d}:" if always_include_hours or hours > 0 else ""
    return (
        f"{hours_marker}{minutes:02d}:{seconds:02d}{ms_marker}"
    )


def video_to_mp3(file_name):
    text = f"ffmpeg -y -i '{file_name}' -vn '{file_name[:-4]}.mp3' "
    print('--> ffmpeg:', text)
    ret = os.system(text)
    if ret != 0:
        logging.error(f'Fehler bei mp4 -> mp3, returncode: {ret}')
    else:
        text = f'video {file_name} wurde erfolgreich nach mp3 konvertiert!'
        #logging.debug(text)

    return ret

def get_duration(file_name):
    text = f'ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "{file_name}"'
    try:
        process = subprocess.Popen(text, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        duration, error = process.communicate()
        if error:
            logging.error(f'Fehler bei get_duration({file_name}): {error.decode("utf-8")}')
    except:
        logging.error(f"{sys.exc_info()[1]}")
        return 0
    print('--> duration:', duration)
    return float(duration)  


# whisper
def transcribe_whisper(audio='', model_size='base', language=None, li_ext=['tvt']):
    logging.debug(f'--> transcribe_whisper({audio}, {model_size})')
    if language == 'en' and model_size in ('tiny', 'base', 'small', 'medium'):
        model_size = f'{model_size}.en'           
    # print(f'--> model_size [{language}] -> {model_size}')        
    model = whisper.load_model(model_size)
    start = time.time()
    if not language: 
        language = None
    try:
        result = model.transcribe(audio, language=language)
    except:
        tout = f"{sys.exc_info()[1]}"
        logging.critical(tout)
        raise Exception(tout)

    lang = result.get('language', 'nn')
    segments = result["segments"]
    dsec = time.time() - start

    #li_ext = ('txt', 'tvt', 'vtt', 'srt', 'lrc')
    for ext in li_ext:
        # write_segments(f'{audio[:-4]}_{model_size}.{lang}.{ext}', segments)
        write_segments(f'{audio[:-4]}.{ext}', segments)

    return segments, dsec 


# faster-whisper
def transcribe_fstr_whisper(audio='', model_size='base', language=None, li_ext=['tvt'], vad_filter=False):
    logging.debug(f'--> transcribe_fstr_whisper({audio}, {model_size})')
    if language == 'en' and model_size in ('tiny', 'base', 'small', 'medium') :
        model_size = f'{model_size}.en'  
    # print(f'--> model_size [{language}] -> {model_size}')      
    model = WhisperModel(model_size, device="cpu", compute_type="int8")
    start = time.time()
    try:
        if language:
            segments_cs, info = model.transcribe(audio, beam_size=5, language=language, vad_filter=vad_filter)
        else:
            segments_cs, info = model.transcribe(audio, beam_size=5, vad_filter=vad_filter)
    except:
        tout = f"{sys.exc_info()[1]}"
        logging.critical(tout)
        raise Exception(tout)

    segements = list(segments_cs)
    #li_ext = ('txt', 'tvt', 'vtt', 'srt', 'lrc')
    for ext in li_ext:
        # write_segments(f'{audio[:-4]}_{model_size}.{lang}.{ext}', segments)
        write_segments(f'{audio[:-4]}.{ext}', segements)

    dsec = time.time() - start
    return segements, dsec 


def write_segments(fname, segments):
    # schreibt das transkribierte in verschiedenen Dateiformaten
    #print('--> write_segments:', fname)
    extension = fname[-3:]
    with open(fname, 'w') as f:
        i = 0
        if extension == 'vtt':
            f.write("WEBVTT\n\n")
        #print('--> t', t, dir(segments))
        for segment in segments:
            i += 1
            #print(segment)
            if type(segment) is dict:
                start = segment['start']
                end = segment['end']
                text = segment['text'].strip()
            else:
                start = segment.start
                end = segment.end
                text = segment.text.strip()

            if extension == 'raw':
                liWords = text.split(' ')
                for word in liWords:
                    f.write(f'{word.strip()}\n')
            else:
                #raw = f"{text} "
                txt = f"{text}\n"
                tvt = f"[{format_timestamp(start)}  {int(end-start)}] {text}\n"
                tvt = f"{format_timestamp(start)}  {text}\n"
                lrc = f"[{format_timestamp(start, True)}] {text}\n"
                vtt = f"{format_timestamp(start, True)} --> {format_timestamp(end, True)}\n{text.replace('-->', '->')}\n\n"
                srt = f"{i}\n{vtt}"
                if extension == 'tvt':
                    f.write(tvt)
                elif extension == 'vtt':
                    f.write(vtt)
                elif extension == 'srt':
                    f.write(srt)
                elif extension == 'lrc':  
                    f.write(lrc)      
                else:
                    f.write(txt)


def main():
    hessisch = os.path.join(pathlib.Path(__file__).parent, 'Thorsten-Voice_Hessisch-Sample1.mp3')
    hochdeutsch = os.path.join(pathlib.Path(__file__).parent / 'Thorsten-Voice_hochdeutsch_piper.mp3')

    li_model_size = ('tiny', 'base', 'small', 'medium', 'turbo')

    audio = hochdeutsch 
    if os.path.exists(audio):
        print(f'-> audio {audio} ist da.\n')
        #transcribe_fstr_whisper(audio, 'medium', language='en')
        #transcribe_fstr_whisper(de_1min, 'small', language='de')
        #transcribe_fstr_whisper(audio, 'tiny', language='de')
        transcribe_whisper(audio, 'tiny', language='de')
    else:
        print(f'-> {audio} fehlt!!')


if __name__ == "__main__":
    start = datetime.datetime.now()
    print('Start: ', start)
    main()
    stop = datetime.datetime.now()
    print('Stop:', stop)
    print(stop-start)
    logging.debug(f'\nFertig!!!')
